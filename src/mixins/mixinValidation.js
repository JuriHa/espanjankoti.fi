
export default {

  data () {
    return {

      // Stop from VALIDATION at the first INPUT editing
      mixinFlagAllowValidate: {
        fullName:              false,
        emailAddress:          false,
        phoneNumber:           false,
        places:                false, // ARRAY
        place:                 false,

        insuranceType:         false,
        insuranceTypeOfficial: false,
        //description:           false,
        companyName:           false,
        pcId:                  false,
        icName:                false,
        registrationNumber:    false,
        passwordOld:           false, // A single PASSWORD INPUT for NOT to work with PASSWORD REPEAT
        password:              false,
        passwordRepeat:        false,
        birthday:              false,
        dateSigning:           false,
        ssn:                   false,
        address:               false,
        iban:                  false,
        priceAnnualEuro:       false,
        priceAnnualCent:       false,
        msg:                   false,
        tradeUnion:            false,
        //recaptcha:             false
      },

      // ERROR MSG of VALIDATION
      mixinValidError: {
        fullName:              '',
        emailAddress:          '',
        phoneNumber:           '',
        places:        '', // ARRAY
        place:        '',

        insuranceType:         '',
        insuranceTypeOfficial: '',
        //description:           '',
        companyName:           '',
        pcId:                  '',
        icName:                '',
        registrationNumber:    '',
        passwordOld:           '', // A single PASSWORD INPUT for NOT to work with PASSWORD REPEAT
        password:              '',
        passwordRepeat:        '',
        birthday:              '',
        dateSigning:           '',
        ssn:                   '',
        address:               '',
        iban:                  '',
        priceAnnualEuro:       '',
        priceAnnualCent:       '',
        msg:                   '',
        tradeUnion:            '',
        //recaptcha:             ''
      },

      // SHOW/HIDE ERROR BOX
      mixinClassVal: {
        fullName:              'display-hide',
        emailAddress:          'display-hide',
        phoneNumber:           'display-hide',
        places:        'display-hide', // ARRAY
        place:        'display-hide',

        insuranceType:         'display-hide',
        insuranceTypeOfficial: 'display-hide',
        //description:           'display-hide',
        companyName:           'display-hide',
        pcId:                  'display-hide',
        icName:                'display-hide',
        registrationNumber:    'display-hide',
        passwordOld:           'display-hide', // A single PASSWORD INPUT for NOT to work with PASSWORD REPEAT
        password:              'display-hide',
        passwordRepeat:        'display-hide',
        birthday:              'display-hide',
        dateSigning:           'display-hide',
        ssn:                   'display-hide',
        address:               'display-hide',
        iban:                  'display-hide',
        priceAnnualEuro:       'display-hide',
        priceAnnualCent:       'display-hide',
        msg:                   'display-hide',
        tradeUnion:            'display-hide',
        //recaptcha:             'display-hide'
      },

      // For making INPUT as an OPTIONAL:
      // in this case VALIDATION do not give ERROR MSG on empty INPUT, but chech it anyway.
      // Make "function of OPTIONAL INPUTs", where you want to use it.
      mixinValidationOptional: {
        fullName:           false,
        emailAddress:       false,
        phoneNumber:        false,
        
        //description:        false,
        companyName:        false,
        pcId:               false,
        icName:             false,
        registrationNumber: false,
        passwordOld:        false,
        password:           false,
        passwordRepeat:     false,
        birthday:           false,
        dateSigning:        false,
        address:            false,
        iban:               false,
        priceAnnualCent:    false,
        msg:                false,
        tradeUnion:         false
      },

      //valueIsEmptyText: 'Pakollinen tieto';

    } // return
  }, // data


  methods: {
		
	mixinValidation (inputValue, inputType) {

		//var inputType = event.target.name; // To take name of INPUT

    //this.test = inputType;
    //console.log('inputType: ' + inputType);
    //console.log('inputValue: ' + inputValue);

    // Need to allow VALIDATION after finding empty INPUTS at pushing "SUBMIT" button 
		if(this.mixinValidError[inputType]) { this.mixinFlagAllowValidate[inputType] = true; }

		if (this.mixinFlagAllowValidate[inputType]) {


        // Regexs here
        var validRegex = (value) => { 
    
          switch (inputType) { // 
    
            case 'fullName':
                  // At least two words, each word at least 1 character (äöåÄÖÅ allowed)
                  var re = /^(\s*)([a-zäöåA-ZÄÖÅ\.]+)(\s+)(([a-zäöåA-ZÄÖÅ\.]+)(\s*))+$/gm;
                  break;
    
            //case 'description':
            case 'place':
            case 'icName':
            case 'address': 
            case 'insuranceType':
            case 'insuranceTypeOfficial':
            case 'msg':
                  // Match everything but NOT BLANK character
                  var re = /^(\s|\S)*(\S)+(\s|\S)*$/gm;
                  break;

            case 'tradeUnion':
                  // Match at least one character
                  var re = /\S+/gm;
                  break;

            case 'places':
                  return true; // No need to check ARRAY here
                  break;

            case 'companyName':
                  // at least 1 words 2-25 character (äöåÄÖÅ allowed)
                  var re = /^[a-zäöåA-ZÄÖÅ.\s]{2,25}$/gm;
                  break;
    
            case 'pcId':
                  // 7 numbers, then "-", then 1 number
                  var re = /^\s*[0-9]{7}[-][0-9]{1}\s*$/gm;
                  break;
    
            case 'registrationNumber':
                  // 5 or 6 numbers
                  var re = /^[0-9]{5,6}$/gm;
                  break;              
    
            case 'emailAddress': // "Allow spaces at the start/end" is added
                  // at least 1 number, char. or ".%+-", then "@", then at least 1 number, char. or ".-", then ".", then at least 2 char.
                  var re = /^\s*[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}\s*$/;
                  //var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                  break;
    
            case 'password':
            case 'passwordOld':
                  // 8-30 characters, at least 1 upper- and 1 lowercase letters, 1 number and 1 special character
                  var re = /^(?=.*[a-zäöå])(?=.*[A-ZÄÖÅ])(?=.*\d)[A-Za-z\d$@!%*?&#_]{8,30}$/;
                  //var re = /^(?=.*[a-zäöå])(?=.*[A-ZÄÖÅ])(?=.*\d)(?=.*[$@!%*?&#_])[A-Za-z\d$@!%*?&#_]{8,30}$/;
                  break;
    
            case 'passwordRepeat': // Just comparing values
                  if (inputValue.password == inputValue.passwordRepeat) {return true;} else {return false;}
                  break;

            case 'birthday':
            case 'dateSigning':
                  // at least 6 numbers or char. or "/.,-\b()".
                  var re = /^[0-9a-zäöå\/\-\s().,]{6,}$/gi;
                  break;

            case 'phoneNumber':
                  // at least 5 numbers or "*/.,+-\b()".
                  var re = /^[0-9\/\-\s().,+*]{5,}$/;
                  break;

            case 'ssn':
                  // DDMMYY, then "+" or "-" or "A", the 3 digits, then one digit or one chars from "A-FHJ-NPR-Y"
                  var re = /^(0[1-9]|[1-2]\d{1}|3[0-1])(0[1-9]|1[0-2])(\d{2})([+\-A])(\d{3})([0-9A-FHJ-NPR-Y])$/;
                  if (re.test(value)) {
                    let check     = [0,1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F','H','J','K','L','M','N','P','R','S','T','U','V','W','X','Y'];
                    let lastChar  = value.slice(-1);
                    let dataNnn   = value.slice(0,6) + value.slice(7,10);
                    let remainder = dataNnn % 31;
                    //console.log('dataNnn ' + dataNnn);
                    //console.log('lastChar ' + lastChar);
                    //console.log('remainder ' + remainder);
                    //console.log('check.remainder ' + check[remainder]);               
                    if (check[remainder] == lastChar) { return true; }
                    else { return false; }
                  }
                  else { return false; }
                  break;

            case 'iban':
                  // "fi" (insensitive), then 16 numbers (any whitespaces are allowed, REGEX counts only numbers).
                  var re = /^\s*(fi)(\s*[0-9]{1}\s*){16}$/i;
                  break;

            case 'priceAnnualEuro':
            case 'priceAnnualCent': /* MAX 2 numbers is checked into INPUTs by maxlength="2" */
                  // at least 1 number
                  var re = /^[0-9]{1,}$/;
                  break;

            default: console.log('ERROR! mixinValidation: inputType \"' + inputType + '\" was not found');
          
          }

          // OBJECT
          if (inputType == 'password' || inputType == 'passwordRepeat') {
            return re.test(value[inputType]);
          }
          // STRING
          else {
            return re.test(value);
          }

        }


        // Checking for emptyness
        var valueIsEmpty = () => {
          // ARRAY
          if (inputType == 'places') { return inputValue.length == 0; }
          // OBJECT
          else if (inputType == 'password' || inputType == 'passwordRepeat') { return !inputValue[inputType]; }
          // STRING
          else { return !inputValue; }
        }


        // Start to check INPUTs for VALIDATION:

        // If INPUT is empty
        if(valueIsEmpty()) {
          this.mixinValidationMsgEmptyText(inputType); // Set Error MSG at empty INPUT
          if (this.mixinValidationOptional[inputType]) { this.mixinValidError[inputType] = ''; } // If INPUT is set as an OPTIONAL: no need in ERROR

        } 
        else { // If INPUT is NOT empty
          
          if (!validRegex(inputValue)) { // Check for REGEX
            
            switch (inputType) { 
            
              case 'fullName':           this.mixinValidError[inputType] = 'Tarkista nimi ja sukunimi';         break;
              case 'icName':             this.mixinValidError[inputType] = 'Tarkista vakuutusyhtiön nimi';      break;
              case 'place':              this.mixinValidError[inputType] = 'Liian lyhyt nimi';                  break;
              case 'insuranceTypeOfficial':
              case 'insuranceType':      this.mixinValidError[inputType] = 'Tarkista vakuutuksen nimi';         break;
              case 'companyName':        this.mixinValidError[inputType] = 'Tarkista yrityksen nimi';           break;
              case 'pcId':               this.mixinValidError[inputType] = 'Tarkista Y-tunnus';                 break;
              case 'registrationNumber': this.mixinValidError[inputType] = 'Tarkista rekisterinumero';          break;
              case 'emailAddress':       this.mixinValidError[inputType] = 'Tarkista sähköpostiosoite';         break;
              case 'password':
              case 'passwordOld':        this.mixinValidError[inputType] = 'Tarkista salasana';                 break;
              case 'passwordRepeat':     this.mixinValidError[inputType] = 'Salasanat eivät täsmää'; 
                                         this.mixinValidError.password   = '';
            	                           this.mixinClassVal.password     = '';                                  break;
              case 'phoneNumber':        this.mixinValidError[inputType] = 'Tarkista puhelinnumero';            break;
              case 'address':            this.mixinValidError[inputType] = 'Tarkista osoitteesi';               break;              
              case 'ssn':                this.mixinValidError[inputType] = 'Tarkista henkilötunnus';            break;              
              case 'birthday':           this.mixinValidError[inputType] = 'Tarkista syntymäpäivä';             break;
              case 'dateSigning':        this.mixinValidError[inputType] = 'Tarkista päivämäärä';               break;
              case 'iban':               this.mixinValidError[inputType] = 'Tarkista tilinumero';               break;
              case 'priceAnnualEuro':    this.mixinValidError[inputType] = 'Käytä euroissa vain numeroita';     break;
              case 'priceAnnualCent':    this.mixinValidError[inputType] = 'Käytä senteissä vain numeroita';    break;
              case 'msg':                this.mixinValidError[inputType] = 'Tarkista viesti';                   break;
              case 'tradeUnion':         this.mixinValidError[inputType] = 'Ammattiliiton nimi on liian lyhyt'; break;
              //case 'description':        this.mixinValidError[inputType] = 'Tarkista viesti';                   break;

              default: alert('ERROR! mixinValidation: inputType \"' + inputType + '\" was not found'); 

            }

          } 

          else {          
            if ( inputType == 'password' && inputValue.passwordRepeat ) {
            	if ( inputValue.passwordRepeat !== inputValue.password ) {
            		this.mixinValidError.password         = 'Salasanat eivät täsmää';
            	    this.mixinValidError.passwordRepeat = '';
            	    this.mixinClassVal.passwordRepeat   = '';
            	}
            	else {
            		this.mixinValidError.password         = '';
            	    this.mixinValidError.passwordRepeat = '';
            	    this.mixinClassVal.passwordRepeat   = '';
                }                
            }
            
            else if ( inputType == 'passwordRepeat' && inputValue.password && inputValue.passwordRepeat == inputValue.password) {
            		this.mixinValidError.passwordRepeat = '';
            	    this.mixinValidError.password     = '';
            	    this.mixinClassVal.password       = '';
            }

            else { this.mixinValidError[inputType] = ''; } // Remove all ERRORs 
          } 

        }
        
        // If there are ERRORs it shows ERROR BOX
        (this.mixinValidError[inputType]) ? this.mixinClassVal[inputType] = 'validation' : this.mixinClassVal[inputType] = '';
    }
	},

    //--- Allow to CHILD (boxValidation) do its job: give INPUT NAME to CHILD to compare it with inputType -------------
    mixinAllowValidate (inputValue, inputType) { 

    	this.mixinFlagAllowValidate[inputType] = true;
    	this.mixinValidation(inputValue, inputType);

    },

    //--- Show error MSG at empty INPUTs -----------------------------------------------------------------------------------------
    mixinIsEmptyBeforeSubmit (inputValue) { 

    	for (let inputType in inputValue) {
        if (inputType == 'places' && inputValue[inputType].length == 0) { // ARRAY
            this.mixinValidError[inputType] = 'Lisää vähintään yksi paikka';
            this.mixinClassVal[inputType]   = 'validation-box-2 m_t-10'; // TODO: Need to find out better solution - without "m_t-10"
        }
        else if (inputValue[inputType] == '' && !this.mixinValidationOptional[inputType]) { // If INPUT is empty and NOT set as an OPTIONAL
          this.mixinValidationMsgEmptyText(inputType); // Set Error MSG at empty INPUT
    			this.mixinClassVal[inputType]   = 'validation';

    		}
      }

    },

    //--- LOCAL WORKER: Set Error MSG at empty INPUT ------------------------------------------------------------------------------
    mixinValidationMsgEmptyText (inputType) {
      switch (inputType) { 
        case 'places':  this.mixinValidError[inputType] = 'Lisää vähintään yksi vakuutus';     break;
        case 'priceAnnualEuro': this.mixinValidError[inputType] = 'Euromäärä ei voi olla tyhjä';       break;
        case 'priceAnnualCent': this.mixinValidError[inputType] = 'Senttimäärä ei voi olla tyhjä';     break;
        //case 'recaptcha':       this.mixinValidError[inputType] = 'ReCaptchan täytyy olla merkattuna'; break;
        default:                this.mixinValidError[inputType] = 'Pakollinen tieto'; //'Tämä kenttä ei voi olla tyhjä'; 
      }
    },

    //---  -----------------------------------------------------------------------------------------
    mixinValidationReset (inputType) {
      this.mixinClassVal[inputType]   = 'display-hide';
      this.mixinValidError[inputType] = '';
    }



  } // methods
}



