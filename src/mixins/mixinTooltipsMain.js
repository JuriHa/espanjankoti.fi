
export default {

  data () {
    return {
      
      // Show TOOLTIP
      mixinTooltip: {
        fullName:           false,
        companyName:        false,
        //companyNamePp:      false,
        birthday:           false,
        pcId:               false,
        registrationNumber: false,
        password:           false,
        iban:               false
      },

      // Show TOOLTIP TEXT
      mixinTooltipText: {
        fullName:                      'Kirjoita ainakin yksi etunimi sekä sukunimi',
        fullNameCompanyRepresentative: 'Kirjoita yrityksen edustajan etu- ja sukunimi',
        companyName:                   'Kirjoita 2-25 merkkiä',
        //companyNamePp:                 'Halutessasi voit lisätä myös toiminimesi tai kevytyrityksesi vakuutukset profiiliisi. Kirjoita 2-25 merkkiä.',
        birthday:                      'Teksti on tulossa...',
        pcId:                          'Kirjoita muodossa 1234567-8',
        registrationNumber:            '5-6 numeroinen tunnus',
        password:                      'Salasanassa on oltava 8-30 merkkiä, ja sen tulee sisältää vähintään yksi iso ja pieni kirjain sekä numero. Erikoismerkeistä hyväksyttyjä ovat $, @, !, %, *, ?, & , _ ja #',
        iban:                          'Vain suomalaiset IBAN-muotoiset tilinumerot ovat sallittuja.'
      },

      // Show of hide TOOLTIP BOX // TODO: "tooltip" was changed, this part need to be deleted after testing the new code of "tooltip"
      mixinTooltipClass: {
        fullName:           'display-hide',
        companyName:        'display-hide',
        pcId:               'display-hide',
        registrationNumber: 'display-hide',
        password:           'display-hide',
        iban:               'display-hide'
      }

    } // return
  }, // data


  methods: {

    //--- Show TOOLTIPS BOX -----------------------------------------------------------------------------------
    mixinTooltipShow (type) {
      this.mixinTooltipClass[type] = 'tooltip';
      //setTimeout(() => {this.mixinTooltipClass[type] = 'tooltip';}, 300);
    },

    //--- Show TOOLTIPS BOX -----------------------------------------------------------------------------------
    mixinTooltipShowSimple (type) {
      this.mixinTooltip[type] = true;
      //this.mixinTooltipClass[type] = 'tooltip';
    },

    //--- Hide TOOLTIPS BOX -----------------------------------------------------------------------------------
    mixinTooltipHide (type) {
      this.mixinTooltip[type] = false;
      //setTimeout(() => { this.mixinTooltipClass[type] = 'display-hide'; }, 300);
    },

    //--- Hide TOOLTIPS BOX -----------------------------------------------------------------------------------
    mixinTooltipShowHideByButton (inputTypeCurrent) {
      this.mixinTooltip[inputTypeCurrent] = !this.mixinTooltip[inputTypeCurrent];
      for (const inputType in this.mixinTooltip) {
          if (inputType == inputTypeCurrent) { continue; }
          this.mixinTooltip[inputType] = false;
        }
    },

  } // methods
}
