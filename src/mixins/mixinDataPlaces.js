
export default {

  data () {
    return {

    mixinDataPlaces: [ // ALL PLACES for living here

      { id: '0',  name: 'Alcudia'           },
      { id: '1',  name: 'Alicante'          },
      { id: '2',  name: 'Baleaarit'         },
      { id: '3',  name: 'Barcelona'         },
      { id: '4',  name: 'Benalmádena'       },
      { id: '5',  name: 'Cabrera'           },
      { id: '6',  name: 'Cala d\'Or'        },
      { id: '7',  name: 'Costa Blanca'      },
      { id: '8',  name: 'Costa del Sol'     },
      { id: '9',  name: 'El Hierro'         },
      { id: '10', name: 'Elche'             },
      { id: '11', name: 'Formentera'        },
      { id: '12', name: 'Fuengirola'        },
      { id: '13', name: 'Fuerteventura'     },
      { id: '14', name: 'Gran Canaria'      },
      { id: '15', name: 'Ibiza'             },
      { id: '16', name: 'Kanariansaaret'    },
      { id: '17', name: 'La Gomera'         },
      { id: '18', name: 'La Palma'          },
      { id: '19', name: 'Las Palmas'        },
      { id: '20', name: 'Los Pacos'         },
      { id: '21', name: 'Madrid'            },
      { id: '22', name: 'Malaga'            },
      { id: '23', name: 'Mallorca'          },
      { id: '24', name: 'Marbella'          },
      { id: '25', name: 'Menorca'           },
      { id: '26', name: 'Nerja'             },
      { id: '27', name: 'Palma Nova'        },
      { id: '28', name: 'Puerto del Carmen' },
      { id: '29', name: 'Santa Pola'        },
      { id: '30', name: 'Teneriffa'         },
      { id: '31', name: 'Torremolinos'      },
      { id: '32', name: 'Torrevieja'        }

    ],
 
    } // return
  }, // data


  methods: {
  } // methods
}
