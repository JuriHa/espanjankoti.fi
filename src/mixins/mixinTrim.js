
export default {

	methods: {
		
		//--- Delete spaces from start and end -------------------------------------------------------------------
		mixinTrimObject (obj) {

      for (let type in obj) {
        //console.log('TRIM-2 works. Type: ' + typeof obj[type]);
        if (typeof obj[type] == 'string') {
          obj[type] = obj[type].replace(/^\s+|\s+$/gm,'');
        }
      }

    }

	} // methods
}
