
export default {

  data () {
    return {

      mixinFlagAllowValidate:  { place:  false         }, /*mixinValidationAllows*/     // Stop from VALIDATION at the first INPUT editing
      mixinValidError:         { place: ''             }, /*mixinValidationErrorTexts*/ // ERROR MSG of VALIDATION
      mixinClassVal:           { place: 'display-hide' }, /*mixinValidationClasses*/    // SHOW/HIDE ERROR BOX
      mixinValidationOptional: { place:  false         },                               // For making INPUT as an OPTIONAL:

    } // return
  }, // data


  methods: {

		
    //---  -----------------------------------------------------------------------------------------------
	  mixinValidation_place (inputValue/*, inputCurrent*/) { 

      var inputType = 'place';

      // Need to allow VALIDATION after finding empty INPUTS at pushing "SUBMIT" button 
	   	if (this.mixinValidError[inputType]) this.mixinFlagAllowValidate[inputType] = true;

		  if (this.mixinFlagAllowValidate[inputType]) {
        this.mixinValidation_place_Check(inputValue);
        this.mixinValidationErrorBoxShowHide(inputType);
      }

	  },


    //--- Validation starts only after user has filles INPUT for the first time ---------------------------
    mixinValidation_place_Start (inputValue, array) {

      var inputType = 'place';

    	this.mixinFlagAllowValidate[inputType] = true;
    	this.mixinValidation_place(inputValue);
      // this.mixinValidation_place_BeforeAdd(inputValue, array);

    },


    //---  -------------------------------------------------------------------------------------------------
    mixinValidation_place_BeforeAdd (inputValue, array, limit) {

      var inputType = 'place';

      if (this.mixinValidation_placeCurrentIsAlreadyAdded(inputValue, array)) {
        //this.mixinClassVal.place   = 'validation';
        this.mixinValidError.place = 'Tämä asumispaikka on jo lisättynä';
      }
      if (array.length == limit) {
        this.mixinValidError.place = 'Kerralla voi lähettää enintään ' + limit + ' paikkaa';
      }
      this.mixinValidationErrorBoxShowHide(inputType);

    },


    //---  -----------------------------------------------------------------------
    mixinValidation_place_isAddedAny (array) {

      var inputType = 'place';

      if (!array.length) {
        this.mixinValidError.place = 'Lisää ainakin yksi paikka';
        this.mixinValidationErrorBoxShowHide(inputType);
      }

    },

 
    //-----------------------------------------------------------------------------------------------------
    //--- INNER WORKER ------------------------------------------------------------------------------------
    mixinValidation_place_Check (inputValue/*, inputCurrent*/) {

      var inputType = 'place';

      var validRegex = () => { 
        // Match everything but NOT BLANK character, at least 2 symbols
        var re = /^(\s|\S)*(\S)+(\s|\S){1,}$/gm;
        return re.test(inputValue);
      }

      if (!inputValue) { // If empty
        this.mixinValidError[inputType] = 'Kirjoita jotain tähän ensin';
        // this.mixinValidationErrorIfEmpty(inputType);
      } 
      else {
        if (!validRegex()) {
          this.mixinValidError[inputType] = 'Nimi on liian lyhyt';
        }
        // else if (inputValue == inputCurrent.trim()) {
        //   this.mixinValidError[inputType] = 'Tämä asumispaikka on jo lisättynä';
        // }
        else {
          this.mixinValidationCommon_ErrorMsgReset(inputType);
        }
      }
  
    },


    //--- INNER WORKER: Is email was already added? -------------------------------------------------------
    mixinValidation_placeCurrentIsAlreadyAdded (inputValue, array) {

      //alert(array.length);
      let doubled = false;
      if ( array.length > 0 ) {
        array.forEach( value => {
          // console.log(value + ': ' + inputValue);
          if (value.trim() == inputValue.trim()) { doubled = true; /*console.log(doubled);*/ }
        });
      }
      return doubled;

    }


  } // methods

}



