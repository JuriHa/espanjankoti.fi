
export default {

  data () {
    return {

      mixinFlagAllowValidate:  { phoneNumber:  false         }, /*mixinValidationAllows*/     // Stop from VALIDATION at the first INPUT editing
      mixinValidError:         { phoneNumber: ''             }, /*mixinValidationErrorTexts*/ // ERROR MSG of VALIDATION
      mixinClassVal:           { phoneNumber: 'display-hide' }, /*mixinValidationClasses*/    // SHOW/HIDE ERROR BOX
      mixinValidationOptional: { phoneNumber:  false         },                               // For making INPUT as an OPTIONAL:

    } // return
  }, // data


  methods: {
		
    //---  -----------------------------------------------------------------------------------------------
	  mixinValidation_phoneNumber (inputValue) { 

      var inputType = 'phoneNumber';

      // Need to allow VALIDATION after finding empty INPUTS at pushing "SUBMIT" button 
	   	if (this.mixinValidError[inputType]) this.mixinFlagAllowValidate[inputType] = true;

		  if (this.mixinFlagAllowValidate[inputType]) {
        this.mixinValidation_phoneNumber_Check(inputValue);
        this.mixinValidationErrorBoxShowHide(inputType);
      }

	  },

    //--- Validation starts only after user has filles INPUT for the first time ---------------------------
    mixinValidation_phoneNumber_Start (inputValue) {

      var inputType = 'phoneNumber';

    	this.mixinFlagAllowValidate[inputType] = true;
    	this.mixinValidation_phoneNumber(inputValue);

    },
 
    //-----------------------------------------------------------------------------------------------------
    //--- INNER WORKER ------------------------------------------------------------------------------------
    mixinValidation_phoneNumber_Check (inputValue) {

      var inputType = 'phoneNumber';

      var validRegex = () => { 
        // at least 5 numbers or "*/.,+-\b()".
        var re = /^[0-9\/\-\s().,+*]{5,}$/;
        return re.test(inputValue);
      }

      if (!inputValue) { // If empty
        this.mixinValidationErrorIfEmpty(inputType);
      } 
      else {
        if (!validRegex()) {
          this.mixinValidError[inputType] = 'Tarkista puhelinnumero';
        }
        else {
          this.mixinValidationCommon_ErrorMsgReset(inputType);
        }
      }
  
    },

  } // methods

}



