
export default {

  data () {
    return {

      mixinFlagAllowValidate:  { fullName:  false         }, /*mixinValidationAllows*/     // Stop from VALIDATION at the first INPUT editing
      mixinValidError:         { fullName: ''             }, /*mixinValidationErrorTexts*/ // ERROR MSG of VALIDATION
      mixinClassVal:           { fullName: 'display-hide' }, /*mixinValidationClasses*/    // SHOW/HIDE ERROR BOX
      mixinValidationOptional: { fullName:  false         },                               // For making INPUT as an OPTIONAL:

    } // return
  }, // data


  methods: {
		
    //---  -----------------------------------------------------------------------------------------------
	  mixinValidation_fullName (inputValue) { 

      var inputType = 'fullName';

      // Need to allow VALIDATION after finding empty INPUTS at pushing "SUBMIT" button 
	   	if (this.mixinValidError[inputType]) this.mixinFlagAllowValidate[inputType] = true;

		  if (this.mixinFlagAllowValidate[inputType]) {
        this.mixinValidation_fullName_Check(inputValue);
        this.mixinValidationErrorBoxShowHide(inputType);
      }

	  },

    //--- Validation starts only after user has filles INPUT for the first time ---------------------------
    mixinValidation_fullName_Start (inputValue) {

      var inputType = 'fullName';

    	this.mixinFlagAllowValidate[inputType] = true;
    	this.mixinValidation_fullName(inputValue);

    },
 
    //-----------------------------------------------------------------------------------------------------
    //--- INNER WORKER ------------------------------------------------------------------------------------
    mixinValidation_fullName_Check (inputValue) {

      var inputType = 'fullName';

      var validRegex = () => { 
        // At least two words, each word at least 1 character (äöåÄÖÅ allowed)
        let re = /^(\s*)([a-zäöåA-ZÄÖÅ\.]+)(\s+)(([a-zäöåA-ZÄÖÅ\.]+)(\s*))+$/gm;
        return re.test(inputValue);
      }

      if (!inputValue) { // If empty
        this.mixinValidationErrorIfEmpty(inputType);
      } 
      else {
        if (!validRegex()) {
          this.mixinValidError[inputType] = 'Tarkista nimi ja sukunimi';
        }
        else {
          this.mixinValidationCommon_ErrorMsgReset(inputType);
        }
      }
  
    },

  } // methods

}



