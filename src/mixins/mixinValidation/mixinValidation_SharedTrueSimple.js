
/*
It is a kind of common simple validation for DATES
*/

export default {

  data () {
    return {

      // mixinFlagAllowValidate:  { birthday:  false         }, /*mixinValidationAllows*/     // Stop from VALIDATION at the first INPUT editing
      // mixinValidError:         { birthday: ''             }, /*mixinValidationErrorTexts*/ // ERROR MSG of VALIDATION
      // mixinClassVal:           { birthday: 'display-hide' }, /*mixinValidationClasses*/    // SHOW/HIDE ERROR BOX
      // mixinValidationOptional: { birthday:  false         },                               // For making INPUT as an OPTIONAL:

    } // return
  }, // data


  methods: {
		
    //---  ------------------------------------------------------------------------------------
	  mixinValidation_SharedTrueSimple (inputValue, inputType) { 

      if (!inputValue) { // If empty
        this.mixinValidationErrorIfEmpty(inputType);
        /*switch (inputType) {
          case 'termsOfUseAccepted': this.mixinValidError[inputType] = 'Ehdot on hyväksyttävä'; break;
          default: alert('ERROR! mixinValidation_SharedDateSimple_Check: inputType \"' + inputType + '\" was not found'); 
          }*/
      } 
      else {
        this.mixinValidationCommon_ErrorMsgReset(inputType);
      }
  
    },

  } // methods

}



