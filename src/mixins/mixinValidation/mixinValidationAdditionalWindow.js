
export default {

  
  data () {
    return {

      mixinValidationAdditionalWindow: { show: false, clicked: 0 }

    } // return
  }, // data


  methods: {

    //--- For showing additional validation window with timer ----------------------
    //--- Example: obj: { show: false, clicked: 0 },
    mixinValidationAdditionalWindowShow (obj=this.mixinValidationAdditionalWindow, duration=4000) { // COMBINE THIS
      obj.clicked++; 
      obj.show = true;
      setTimeout(() => {
        if (obj.clicked == 1) obj.show = false;
        obj.clicked--;
      }, duration);

    },

    //--- --------------------------------------------------------------------------
    mixinValidationAdditionalWindowClose (obj=this.mixinValidationAdditionalWindow) {
      obj.show    = false;
      //obj.clicked = 0;
    }
    
  } // methods
}
