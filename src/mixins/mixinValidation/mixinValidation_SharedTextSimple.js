
/*
It is a kind of common simple validation for TEXTS.
*/

export default {

  data () {
    return {

      //TODO: For some reason reactivity does not work by adding values into this objects. Need to find out a way to do it.

      mixinFlagAllowValidate: { /*mixinValidationAllows*/ // Stop from VALIDATION at the first INPUT editing
        icName:                false,
        address:               false,
        insuranceTypeOfficial: false,
        insuranceType:         false,
        msg:                   false,
        tradeUnion:            false,
        clientCategory:        false,
      },

      mixinValidError: { /*mixinValidationErrorTexts*/ // ERROR MSG of VALIDATION
        icName:                '',
        address:               '',
        insuranceTypeOfficial: '',
        insuranceType:         '',
        msg:                   '',
        tradeUnion:            '',
        clientCategory:        '',
      },

      mixinClassVal: { /*mixinValidationClasses*/ // SHOW/HIDE ERROR BOX
        icName:                'display-hide',
        address:               'display-hide',
        insuranceTypeOfficial: 'display-hide',
        insuranceType:         'display-hide',
        msg:                   'display-hide',
        tradeUnion:            'display-hide',
        clientCategory:        'display-hide',
      },

      mixinValidationOptional: { // For making INPUT as an OPTIONAL:
        icName:                false,
        address:               false,
        insuranceTypeOfficial: false,
        insuranceType:         false,
        msg:                   false,
        tradeUnion:            false,
        clientCategory:        false,
      }

    } // return
  }, // data


  methods: {
		
    //---  -----------------------------------------------------------------------------------------------
	  mixinValidation_SharedTextSimple (inputValue, inputType) {

      // Need to allow VALIDATION after finding empty INPUTS at pushing "SUBMIT" button 
	   	if (this.mixinValidError[inputType]) this.mixinFlagAllowValidate[inputType] = true;

		  if (this.mixinFlagAllowValidate[inputType]) {
        this.mixinValidation_SharedTextSimple_Check(inputValue, inputType);
        this.mixinValidationErrorBoxShowHide(inputType);
        //console.log('mixinValidation_SharedTextSimple: ' + this.mixinValidError[inputType]);
        //console.log('mixinValidation_SharedTextSimple: ' + this.mixinClassVal[inputType]);
      }

	  },

    //--- Validation starts only after user has filles INPUT for the first time ---------------------------
    mixinValidation_SharedTextSimple_Start (inputValue, inputType) {
      
      //this.mixinValidationCommon_ValueAdd(inputType); // In use only for SHARED mixins
    	this.mixinFlagAllowValidate[inputType] = true;
    	this.mixinValidation_SharedTextSimple(inputValue, inputType);

    },
 
    //-----------------------------------------------------------------------------------------------------
    //--- INNER WORKER ------------------------------------------------------------------------------------
    mixinValidation_SharedTextSimple_Check (inputValue, inputType) {

      var validRegex = () => {
        // Match everything but NOT BLANK character, at least 2 symbols
        var re = /^(\s|\S)*(\S)+(\s|\S){1,}$/gm;
        return re.test(inputValue);
      }

      if (!inputValue) { // If empty
        this.mixinValidationErrorIfEmpty(inputType);
      } 
      else {
        if (!validRegex()) {
          switch (inputType) { 
            case 'icName':                this.mixinValidError[inputType] = 'Tarkista vakuutusyhtiön nimi'; break;
            case 'address':               this.mixinValidError[inputType] = 'Tarkista osoitteesi'; break;
            case 'insuranceTypeOfficial': 
            case 'insuranceType':         this.mixinValidError[inputType] = 'Tarkista vakuutuksen nimi'; break;
            case 'msg':                   this.mixinValidError[inputType] = 'Tarkista viesti'; break;
            case 'tradeUnion':            this.mixinValidError[inputType] = 'Ammattiliiton nimi on liian lyhyt'; break;
            case 'clientCategory':        this.mixinValidError[inputType] = 'Tarkista asiakasryhmä'; break;
            default: alert('ERROR! mixinValidation_SharedTextSimple_Check: inputType \"' + inputType + '\" was not found');
          }
        }
        else {
          this.mixinValidationCommon_ErrorMsgReset(inputType);
        }
      }
  
    },

  } // methods

}



