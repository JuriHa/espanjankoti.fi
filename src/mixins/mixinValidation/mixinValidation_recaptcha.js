
export default {

  data () {
    return {
    } // return
  }, // data


  methods: {
		
    //--- ----------------------------------------------------------------------------------------
    mixinValidation_recaptcha (recaptcha) {

      if (!recaptcha) {
        this.mixinValidationServerBasicShow = true; // SHOWs validation box
        this.mixinValidationServerBasicMessages.push('ReCAPTCHA virhe. Päivitä sivu ja yritä uudelleen.');
      }

    }

  } // methods

}



