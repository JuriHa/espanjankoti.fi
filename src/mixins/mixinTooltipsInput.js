
export default {

  data () {
    return {
      
      // Show TOOLTIP
      mixinTooltipsInput: {
        fullName:    false,
        phoneNumber: false
      },

      // Show TOOLTIP TEXT
      mixinTooltipsInputText: {
        fullName:    'Kirjoita ainakin yksi etunimi sekä sukunimi',
        phoneNumber: 'Kirjoita se muodossa 012 34567890'
      },

      // Show of hide TOOLTIP BOX // TODO: "tooltip" was changed, this part need to be deleted after testing the new code of "tooltip"
      mixinTooltipsInputClass: {
        fullName:    'display-hide',
        phoneNumber: 'display-hide'
      }

    } // return
  }, // data


  methods: {


    //--- Show TOOLTIPS BOX -----------------------------------------------------------------------------------
    mixinTooltipsInputShow (type) {
      this.mixinTooltipsInputClass[type] = 'tooltip';
      //setTimeout(() => {this.mixinTooltipsInputClass[type] = 'tooltip';}, 300);
    },


    //--- Show TOOLTIPS BOX -----------------------------------------------------------------------------------
    mixinTooltipsInputShowSimple (type) {
      this.mixinTooltipsInput[type] = true;
      //this.mixinTooltipsInputClass[type] = 'tooltip';
    },


    //--- Hide TOOLTIPS BOX -----------------------------------------------------------------------------------
    mixinTooltipsInputHide (type) {
      this.mixinTooltipsInput[type] = false;
      //setTimeout(() => { this.mixinTooltipsInputClass[type] = 'display-hide'; }, 300);
    },


    //--- Hide TOOLTIPS BOX -----------------------------------------------------------------------------------
    mixinTooltipsInputShowHideByButton (inputTypeCurrent) {

      this.mixinTooltipsInput[inputTypeCurrent] = !this.mixinTooltipsInput[inputTypeCurrent];
      for (const inputType in this.mixinTooltipsInput) {
          if (inputType == inputTypeCurrent) { continue; }
          this.mixinTooltipsInput[inputType] = false;
        }

    },


  } // methods
}
