
//import { store } from '@/store' // Because this.$store.commit('userTypeReset') do not work here


export default {

	methods: {
		
	  //--- ONLY! for: "post", "asynchronous" and "application/x-www-form-urlencoded" XMLHttpRequests ----------
	  mixinXhrPostAsyncApp (obj, url, header) {
            
      var catchErrors = (isOnerror) => {
        //store.commit('requestFailedMainSet');
        if (isOnerror) { console.log(url + "ONERROR: An error occurred during the transaction"); }
        else           { console.log('REQUEST STATUS (not 4&200): ' + url + ': ' + xhr.status + ', ' + xhr.readyState); }
      }

      if (url) {

        // Create from SERVER response user friendly ERROR MSGs
        // "url" need when there is 2 or more requests from the same file
        var responseMessages = (response, url) => { this.xhrPostAsyncAppResponseMsgs(response, url); }

        var xhr = new XMLHttpRequest();
            
        xhr.onreadystatechange = function() {
          if (xhr.readyState == 4 && xhr.status == 200) {
            console.log(url + ': B response as string: ' + xhr.response); // SERVER RESPONSE as STRING
            var response = JSON.parse(xhr.response);
            responseMessages(response, url); // Need brecause cannot call from here xhrPostAsyncAppResponseMsgs function
          } 
          else if (xhr.readyState == 4 && xhr.status >= 400) { catchErrors(); }
        };
            
        xhr.open('POST', '/' + url, true);

        xhr.onerror = () => { catchErrors(true); };

        //if    (header == 'app')       { xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8'); }
        if      (header == 'app')       { xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); }
        else if (header == 'multipart') { xhr.setRequestHeader('Content-Type', 'multipart/form-data'); }

        //console.log(url + ": F request (JSON.stringify(obj)): " + JSON.stringify(obj)); // Show request
        console.log(url + ': F request (' + typeof obj + '): ' + obj); // Show request

        xhr.send(obj);
          
      }

	  },
        

	} // methods
}
