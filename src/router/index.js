
import Vue         from 'vue'
import Router      from 'vue-router'
import pageInit    from '@/components/pageInit'
import pageObjects from '@/components/pageObjects'


Vue.use(Router)


export default new Router({
  mode: 'history',
  routes: [
    
    {path: '/',        name: 'pageInit',    component: pageInit },
    {path: '/objects', name: 'pageObjects', component: pageObjects },

  ]
})
